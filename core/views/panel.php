<nav class="navbar navbar-default">
      <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Bitsfer</a>
          </div>
        
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             
              <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <!--<img src="assets/images/avatar.png" alt="avatar" id="user-avatar">-->
                        <span id="login-info"></span><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu">
                          <li><a href="#index/default/logout">Wyloguj się</a></li>
                      </ul>
                  </li>
              </ul>
          </div><!-- /.navbar-collapse -->
      </div>
    </nav>
        
    <div class="container">
        <div class="small-gap"></div>
        
        <section id="content">
            <ul class="nav nav-tabs" role="tablist">
                <li><a href="/#panel/dashboard"> <i class="fa fa-inbox"></i> Dashboard</a></li>
                <li><a href="/#panel/wallet"> <i class="fa fa-briefcase"></i> Portfele</a></li>
                <li><a href="/#panel/exchange"> <i class="fa fa-refresh"></i> Giełda</a></li>
                <li><a href="/#panel/history"> <i class="fa fa-mail-reply"></i> Historia</a></li>
                <li><a href="/#panel/settings"> <i class="fa fa-cog"></i> Ustawienia</a></li>
               <!-- <div class="special-links">
                  <li><a href="#"> <i class="fa fa-mobile-phone" style="font-size: 20px;"></i> <span>Doładuj<br>telefon</span></a></li>
                  <li><a href="#"> <i class="fa fa-file-text-o"></i> <span>Zapłać<br>rachunek</span></a></li>
                  <li><a href="#"> <i class="fa fa-dashboard"></i> <span>Konta<br>premium</a></span></li>
                </div>-->
            </ul>
            <div class="notifications bottom-right"></div>

            <div class="Controller-Content"></div>
            
            <script type="text/javascript" src="assets/js/bootstrap.notify.js"></script>

            <div class="medium-gap"></div>
        </section><!-- /#content -->
        
        <section id="banner">
          <button class="btn btn-bordered btn-primary">Załóż konto PREMIUM!</button>
        </section>
    </div>
    <div class="gap"></div>
    <footer>
      <div class="container">
          <strong class="info-number">
          <i class="fa fa-mobile-phone very-large"></i>801 100 500 <br><span class="number-description">Centrum obsługi klienta</span></strong>
          <span class="copy">BitOpen <?php echo date("Y"); ?></span>
          
        </div>
      </div>
    </footer>