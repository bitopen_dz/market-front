<script>
  var parts = window.location.hash.split("/"),
      keyParam = parts[parts.length-1],
      key = keyParam.split("-")[1];
  console.info(parts, keyParam, key);
  if(!key || key.length < 10) {
    error("Błędny klucz aktywacyjny");
    window.location.hash = "";
  }
  else {
    rest.post("/auth/activate", function(data) {
      success("Konto zostało aktywowane. Możesz się teraz zalogować");
      window.location.hash = "";
    }, {activationKey: key});
  }
</script>