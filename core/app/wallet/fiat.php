<div id="content-inner">
              <div class="gap"></div>
              <h3><a href="#panel/wallet" class="btn">&laquo; wróć</a> Bieżący portfel</h3>
              <div class="small-gap"></div>
              
              <ul class="accounts-list" id="wallets-list">
                  <p class="text-center">
                        <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
                  </p>
              </ul>
              <div class="row" id="wallet-details" style="display: none;">
                            <div class="col-md-6">
                                          <h3>Saldo dostępne: <span id="available-balance"></span></h3>
                                          <h5>Stan konta: <span id="wallet-balance"></span></h4>
                                          <h5>Środki zablokowane: <span id="locked-balance"></span></h4>
                            </div>
              </div>
              
              
              <div class="small-gap"></div>
              <div class="row">
              <div class="col-md-6 distanced-right">
                           <h3>Wykonaj wpłatę</h3>
                           <form action="#" method="post" id="deposit-form">
                            <table class="table table-form">
                                          <tbody>
                                                        <tr>
                                                                      <td>
                                                                             Kwota wpłaty:      
                                                                      </td>
                                                                      <td>
                                                                             <input type="number" class="form-control" id="amount">
                                                                      </td>
                                                        </tr>
                                                        <tr>
                                                                      <td></td>
                                                                      <td>
                                                                                     <button class="btn btn-primary">Wpłać</button>
                                                                      </td>
                                                        </tr>
                                          </tbody>
                            </table>
                           </form>
                           <h3>Historia wpłat</h3>
                      <div class="small-gap"></div>
                      <table class="table table-history" id="deposits-history">
                            <tbody>
                                          <tr>
                                            <td colspan="4" class="text-center">
                                                        <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
                                            </td>
                                          </tr>
                            </tbody>
                      </table>
                           
              </div>
                  <div class="col-md-6 distanced-right">
                      <h3>Wykonaj wypłatę</h3>
                      <div class="small-gap"></div>
                      <form id="withdrawal-form" method="post">
                            <table class="table table-form">
                              <tr>
                                <td class="text-right">Numer konta:</td>
                                <td colspan="2"><input class="form-control" class="text" name="accountNumber"></td>
                              </tr>
                              <tr>
                                <td class="text-right">Kwota: </td>
                                <td><input class="form-control" class="number" step="0.01" name="amount"></td>
                                <td>PLN</td>
                              </tr>
                              <tr>
                                <td class="text-right">Tytułem: </td>
                                <td colspan="2"><input class="form-control" class="text" name="title"></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td colspan="2">
                                  <button class="btn btn-primary">Wykonaj</button>
                                </td>
                              </tr>
                            </table>
                    </form>
                      <h3>Historia wypłat</h3>
                      <div class="small-gap"></div>
                      <table class="table table-history" id="withdrawals-history">
                            <tbody>
                                          <tr>
                                            <td colspan="4" class="text-center">
                                                        <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
                                            </td>
                                          </tr>
                            </tbody>
                      </table>
                  </div>
              </div>
            </div>
<script src="js/app/wallet/fiat.js"></script>
            