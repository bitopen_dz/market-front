<div id="login-box">
  <nav class="navbar navbar-default">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header text-center">
          <a class="navbar-brand" href="#">Bitsfer</a>
      </div>
  </nav>
  <section id="subheader" class="row">
    <form action="#!" id="register-form">
      <table class="table table-form">
        <tr>
          <td class="text-center"><input type="text" class="form-control" placeholder="Adres e-mail" id="register-login" /></td>
        </tr>
        <tr>
          <td class="text-center"><input type="password" class="form-control" placeholder="Hasło" id="register-password"></td>
        </tr>
        <tr>
          <td class="text-center"><input type="password" class="form-control" placeholder="Powtórz hasło" id="register-repeat-password"></td>
        </tr>
        <tr>
          <td class="text-center">
            <a href="#index/default" class="btn">Wróć do logowania</a>
            <button class="btn btn-primary">Załóż konto</button>
          </td>
        </tr>
      </table>
    </form>
  </section>
      
  <footer>
    <strong class="info-number">
            
    </strong>
  </footer>
</div>
<script src="js/app/default/register.js"></script>

