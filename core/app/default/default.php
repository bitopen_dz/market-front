<div id="login-box">
  <nav class="navbar navbar-default">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header text-center">
          <a class="navbar-brand" href="#">Bitsfer</a>
      </div>
  </nav>
  <section id="subheader" class="row">
    <form action="#!" id="login-form">
      <table class="table table-form">
        <tr>
          <td class="text-center"><input type="text" class="form-control" placeholder="Adres e-mail" id="login-login" /></td>
        </tr>
        <tr>
          <td class="text-center"><input type="password" class="form-control" placeholder="Hasło" id="login-password"></td>
        </tr>
        <tr>
          <td class="text-center">
            <a href="#index/default/register" class="btn ">Nie masz jeszcze konta?</a>
            <button class="btn btn-primary">Zaloguj</button>
          </td>
        </tr>
      </table>
    </form>
  </section>
      
  <footer>
    <strong class="info-number">
            
    </strong>
  </footer>
</div>
<script src="js/app/default/default.js"></script>

