<div id="content-inner">
  <div class="gap"></div>
  <h3>Rynek BTC / PLN</h3>
  <div class="small-gap"></div>
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-6"><h4>Sprzedaj</h4></div>
          <div class="col-md-6 text-right" style="padding-top: 10px;">Saldo dostępne: <span class="fill-ask-amount" id="BTC-balance">-</span></div>
        </div>
        
        <form id="ask-action">
          <table class="table">
            <tr>
              <td>Kurs: <input id="ask-rate" type="text" class="form-control" /></td>
              <td>Ilość: <input id="ask-amount" type="text" class="form-control" /></td>
              <td>Cena: <input id="ask-price" type="text" class="form-control" /></td>
              <td><br /><button class="btn btn-success">Sprzedaj</button></td>
            </tr>
          </table>
        </form>
        <h4>Oferty kupna (BID)</h4>
        <table class="table table-scrolled table-3-cols" id="bid-table">
          <thead>
            <tr>
              <th>Kurs (PLN)</th>
              <th>Ilość (BTC)</th>
              <th>Cena (PLN)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="3" class="text-center"><span class="fa fa-spinner fa-pulse fa-3x text-center"></span></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-6"><h4>Kup</h4></div>
          <div class="col-md-6 text-right" style="padding-top: 10px;">Saldo dostępne: <span class="fill-bid-price" id="PLN-balance">-</span></div>
        </div>
        <form id="bid-action">
          <table class="table">
            <tr>
              <td>Kurs: <input id="bid-rate" type="text" class="form-control" /></td>
              <td>Ilość: <input id="bid-amount" type="text" class="form-control" /></td>
              <td>Cena: <input id="bid-price" type="text" class="form-control" /></td>
              <td><br /><button class="btn btn-danger">Kup</button></td>
            </tr>
          </table>
        </form>
        <h4>Oferty sprzedaży (ASK)</h4>
        <table class="table table-scrolled table-3-cols" id="ask-table">
          <thead>
            <tr>
              <th>Kurs (PLN)</th>
              <th>Ilość (BTC)</th>
              <th>Cena (PLN)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="3" class="text-center"><span class="fa fa-spinner fa-pulse fa-3x text-center"></span></td>
            </tr>
            
          </tbody>
        </table>
      </div>
      <div class="col-md-12" style="display: none;" id="offers-div">
        <h4>Twoje aktywne oferty</h4>
        <table class="table table-scrolled table-5-cols" id="offers-table">
          <thead>
            <tr>
              <th>Typ</th>
              <th>Kurs</th>
              <th>Ilość</th>
              <th>Cena</th>
              <th>Akcje</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
      <div class="col-md-12">
        <h4>Ostatnie transakcje</h4>
        
        <table class="table table-scrolled table-4-cols" id="transactions-table">
          <thead>
            <tr>
              <th>Data</th>
              <th>Kurs (PLN)</th>
              <th>Ilość (BTC)</th>
              <th>Cena (PLN)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="3" class="text-center"><span class="fa fa-spinner fa-pulse fa-3x text-center"></span></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    
  </div>
</div>

<script src="js/app/exchange/default.js"></script>