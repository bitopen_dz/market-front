<div id="content-inner">
              <div class="gap"></div>
              <h3>Twoje portfele</h3>
              <div class="small-gap"></div>

              <ul class="accounts-list" id="wallets-list">
                  <p class="text-center">
                        <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
                  </p>
               <!-- <li>


                  <div class="accounts-list-item-basics col-md-7 col-sm-12">
                    <strong>Konto Basic</strong>
                    <span class="hidden-xs">55 9723 3293 2232 8879 5820</span>
                    <strong class="visible-sm visible-xs pull-right">16 123,13</strong>
                  </div>
                  <div class="accounts-list-item-balance col-md-5 hidden-sm hidden-xs">
                    <span>Dostępne środki:</span>
                    <strong>16 123,13</strong>
                    <em>BTC</em>
                  </div>
                </li>-->
              </ul>

              <div class="small-gap"></div>
              <div class="row">
                  <div class="col-md-12">
                      <h3>Ostatnie operacje</h3>
                      <div class="small-gap"></div>
                      <table class="table table-history" id="history-table">
                            <tbody>
                                          <tr>
                                            <td colspan="4" class="text-center">
                                                        <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
                                            </td>
                                          </tr>
                            </tbody>
                            <tfoot>
                                          <tr>
                                            <td colspan="4" class="text-center">
                                              <div class="small-gap"></div>
                                              <a href="#panel/history" class="btn btn-primary">Zobacz pełną historię</a>
                                            </td>
                                          </tr>
                            </tfoot>
                      </table>
                  </div>
              </div>
            </div>
            
<script src="js/app/dashboard/default.js"></script>