<div id="content-inner">
<div class="gap"></div>

  <h3>Historia operacji</h3>
  <div class="small-gap"></div>
  <table class="table table-history" id="history-table">
    <thead>
      <tr>
        <th></th>
        <th>Typ</th>
        <th>Kwota operacji</th>
        <th>Stan konta po operacji</th>
        <th>Data</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="4" class="text-center">
          <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
        </td>
      </tr>
    </tbody>
  </table>
  <div id="pagination" class="text-center"></div>
</div>
            
<script src="js/app/history/default.js"></script>