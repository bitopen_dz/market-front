<div id="content-inner">
              <div class="gap"></div>
              <h3>Administracja - użytkownicy</h3>
              <div class="row">
                  <div class="col-md-12">
                      <input type="text" class="form-control" id="users-filter" placeholder="Wyszukaj..."/>
                      <table class="table table-history" id="users-table">
                          <thead>
                              <tr>
                                  <th>Adres e-mail</th>
                                  <th>Status</th>
                                  <th>Data utworzenia konta</th>
                                  <th>Akcje</th>
                              </tr>
                          </thead>
                            <tbody>
                              <tr>
                                <td colspan="4" class="text-center">
                                            <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
                                </td>
                              </tr>
                            </tbody>
                      </table>
                  </div>
              </div>
            </div>
            
<script src="js/app/admin/users.js"></script>