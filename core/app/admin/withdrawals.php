<div id="content-inner">
              <div class="gap"></div>
              <h3>Administracja - wypłaty</h3>
              <div class="row">
                  <div class="col-md-12">
                      <select class="form-control" id="currency">
                          <option>BTC</option>
                          <option>PLN</option>
                      </select>
                      <table class="table table-history" id="withdrawals-table">
                          <thead>
                              <tr>
                                  <th>Data</th>
                                  <th>Kwota</th>
                                  <th>Numer konta</th>
                                  <th>Akcje</th>
                              </tr>
                          </thead>
                            <tbody>
                              <tr>
                                <td colspan="4" class="text-center">
                                            <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
                                </td>
                              </tr>
                            </tbody>
                      </table>
                      <button class="btn btn-default" id="send-withdrawals" style="display: none;">Wyślij powyższe przelewy</button>
                  </div>
              </div>
            </div>
            
<script src="js/app/admin/withdrawals.js"></script>