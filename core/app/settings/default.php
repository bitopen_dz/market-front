<div id="content-inner">
              <div class="gap"></div>
              <div class="row">
                  <div class="col-md-6 distanced-right">
                      <h3>Dwustopniowa autoryzacja</h3>
                            <div id="two-step-verification-status">
                                          
                                          <span class="fa fa-spinner fa-pulse fa-3x text-center"></span>
                            </div>
                  </div>
                  <div class="col-md-6">
                            <h3>Zmiana hasła</h3>
                            <form id="change-password-form" class="row">
                                          <div class="col-md-5">
                                                        <input type="password" class="form-control" id="new-password" placeholder="Nowe hasło" />
                                          </div>
                                          <div class="col-md-5">
                                                        <input type="password" class="form-control" id="new-password-repeat" placeholder="Powtórz nowe hasło" />
                                          </div>
                                          <div class="col-md-2">
                                                        <button class="btn btn-primary">Zapisz</button>
                                          </div>
                            </form>
                  </div>
                  <div class="col-md-12">
                            <h3>Edycja danych osobowych</h3>
                            <form class="row" id="user-details-form">
                                          <table class="table">
                                                        <tr>
                                                                      <td><input required name="firstname" class="form-control" type="text" placeholder="Imię"/></td>
                                                                      <td><input required name="lastname" class="form-control" type="text" placeholder="Nazwisko"/></td>
                                                        </tr>
                                                        <tr>
                                                                      <td><input required name="street1" class="form-control" type="text" placeholder="Adres (część 1)"/></td>
                                                                      <td><input name="street2" class="form-control" type="text" placeholder="Adres (część 2)"/></td>
                                                        </tr>
                                                        <tr>
                                                                      <td><input required name="postcode" class="form-control" type="text" placeholder="Kod pocztowy"/></td>
                                                                      <td><input required name="city" class="form-control" type="text" placeholder="Miasto"/></td>
                                                        </tr>
                                                        <tr>
                                                                      <td><input required name="idcardNumber" class="form-control" type="text" placeholder="Numer dowodu osobistego"/></td>
                                                                      <td><select name="country" class="form-control"><option value="PL">Polska</option></select></td>
                                                        </tr>
                                                        <tr>
                                                                      <td colspan="2" style="text-align: right;">
                                                                                    <button class="btn btn-primary">Zapisz</button>
                                                                      </td>
                                                        </tr>
                                          </table>
                            </form>
                  </div>
              </div>
            </div>
            
<script src="js/app/settings/default.js"></script>