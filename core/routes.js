/**
 * Default routes
 */

var lastView = null;
var limitRoutes = false;
var aviableRoutes = [
  "index/default/default",
  "index/default/dashboard",
  "index/default/register"
];


var Router = Backbone.Router.extend({
  routes: {
    '': "defaultAction",
    ':view': "defaultAction",
    ':view/:controller': "defaultAction",
    ':view/:controller/:action': "defaultAction",
    ':view/:controller/:action/:param': "defaultAction",
    ':view/:controller/:action/:param/:param2': "defaultAction"
  },
  
  defaultAction: function(view, controller, action) {
    window.masterInited = false;
    if (limitRoutes && $.inArray(view+"/"+controller+"/"+action, aviableRoutes) < 0)
    {
      Call404();
    }
    else
    {
      if (typeof(view) == "undefined" || view.length == 0)
      {
        view = 'index';
      }
      if (typeof(controller) == "undefined")
      {
        controller = 'default';
      }
      if (typeof(action) == "undefined")
      {
        action = 'default';
      }
      view = view.split(".").join("/");
      controller = controller.split(".").join("/");
      NProgress.start(); 
      $.ajax({
        url: 'core/app/'+controller+'/'+action+'.php',
        success: function(data) {
          NProgress.set(25);
          if (view == lastView)
            $(".Controller-Content").html(data);
          else
          {
            $.ajax({
              url: 'core/views/'+view+'.php',
              success: function(data_view) {
                NProgress.set(100);
                $("body").html(data_view);
                $(".Controller-Content").html(data);
                lastView = view;
              },
              error: function() {
                NProgress.set(100);
                Call404();
              }
            });
          }
          
        },
        error: function() {
          NProgress.set(100);
          Call404();
        }
      });
    }
  }
});

var Call404 = function()
{
  error("Wystąpił błąd w trakcie ładowania strony");
  //alert("404 -> View not found.");
}

/**
 * Starting appliaction history
 */
Backbone.history.start();
window.Router = new Router();
$(function() {
  var x = window.location.hash.substr(1).split("/");
  window.Router.defaultAction(x[0], x[1], x[2]);
  
})


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
