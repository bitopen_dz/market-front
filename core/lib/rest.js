var rest = {};

rest.headers = {};
rest.prefix = "http://bitopen.pl:9999";
rest.isJson = false;

rest.setHeader = function(name, value){
  rest.headers[name] = value;
}

rest.get = function(url, callback, data, showLoader) {
  rest.call(url, callback, "GET", data, showLoader);
}
rest.post = function(url, callback, data, showLoader) {
  rest.call(url, callback, "POST", data, showLoader);
}
rest.put = function(url, callback, data, showLoader) {
  rest.call(url, callback, "PUT", data, showLoader);
}
rest.delete = function(url, callback, data) {
  rest.call(url, callback, "DELETE", data);
}

rest.call = function(url, callback, method, requestData, showLoader) {
  if(showLoader == undefined || showLoader)
  {
    NProgress.start();
  }
  if(getCookie("Authorization").length > 0){
    rest.setHeader("Authorization", getCookie("Authorization"));
  }
  if(rest.isJson) {
    requestData = JSON.stringify(requestData);
  }
  var input = {
    type: method,
    data: requestData,
    url: rest.prefix + url,
    headers: rest.headers,
    success: function(data) {
      rest.isJson = false;
      if(showLoader == undefined || showLoader)
      {
        NProgress.set(100)
      }
      if(data.status === 0){
        error("Wystąpił błąd: "+data.message);
        if(data.result == "2fa_token_required"){
          bootbox.prompt("Podaj token dwustopniowej autoryzacji", function(result) {
            if(result && result.length > 0) {
              if(typeof(requestData) == "string") {
                requestData = JSON.parse(requestData);
                rest.isJson = true;
              }
              requestData = requestData || {};
              requestData['key'] = result;
              rest.call(url, callback, method, requestData, showLoader);
            }
          });
        }
      }
      else
      {
        callback(data);
      }
    },
    error: function() {
      NProgress.set(100)
      error("Wystąpił błąd");
    }
  }
  if(rest.isJson) {
    input['contentType'] = "application/json; charset=utf-8";
  }
  $.ajax(input);
}

rest.authorize = function(email, password, callback){
  rest.post("/auth/authorization", function(data) {
    success("Zostałeś poprawnie zalogowany");
    setCookie("Authorization", data.result.sessionKey);
    rest.setHeader("Authorization", data.result.sessionKey);
    if(callback){
      callback();
    }
  }, {email: email, password: password});
}

rest.logout = function(callback) {
  rest.get("/auth/logout", function(data) {
    if(callback){
      callback();
    }
  });
  rest.setHeader("Authorization", "");
  setCookie("Authorization", "");
}

function success(msg)
{
  $('.bottom-right').notify({
    message: { text: msg },
    type: 'success'
  }).show();
}
function warning(msg)
{
  $('.bottom-right').notify({
    message: { text: msg },
    type: 'warning'
  }).show();
}
function error(msg)
{
  $('.bottom-right').notify({
    message: { text: msg },
    type: 'danger'
  }).show();
}

