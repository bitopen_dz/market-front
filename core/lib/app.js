window.App = {
  CreateUrl: function(name)
  {
    return "/" + name.split("_").join("/");
  },
  Imported: [],
  Import: function(script, callback)
  {
    if ( $.inArray(script, this.Imported ) < 0 )
    {
      $.getScript( script, callback );
      this.Imported.push(script);
    }
    else
    {
      callback();
    }
  },
  Objects: [],
  RegisterObject: function( object )
  {
    this.Objects.push( object );
    var id = this.Objects.length-1;
    //
    console.log(object.content)
    if (object.content != null && object.content != undefined)
    {
      object.content.attr("data-obj", id);
    }
    else if (object.el != null && object.el != undefined)
    {
      object.el.attr("data-obj", id);
    }
    
    return id;
  },
  GetObject: function( id ) {
    if (!isNaN(parseInt(id)))
    {
      return this.Objects[id];
    }
    else
    {
      return this.Objects[ $(id).attr("data-obj") ];
    }
  }
  
}

function display(number, precision, nan_char) {
  if (typeof(precision) == "object")
  {
    if (typeof(precision.is_crypto) != "undefined")
      precision = 8;
    else
      precision = 2;
    
  }
  if (isNaN(parseFloat(number)) && nan_char != undefined) 
    return nan_char;
  
  return parseFloat(number).toFixed(precision).replace(/0+$/,'').replace(/\.$/,'').replace(".",",");
}

window.User = {}
window.Currencies = {}
window.CurrenciesPairs = {}
window.masterInited = false;

function display(amount, currencyId){
  return parseFloat(amount).toFixed(Currencies[currencyId].accuracy)+" "+currencyId;
}

function masterInit(callback) {
  if(window.masterInited){callback();}
  else{
    rest.get("/auth/check", function(data) {
      window.User = data.result.user;
      $("#login-info").text(User.email);

      if(User.admin) {
        initAdminButtons();
      }

      rest.get("/currencies", function(data) {
        window.Currencies = data.result;
        rest.get("/currencies/pairs", function(data) {
          window.CurrenciesPairs = data.result;
          window.masterInited = true;
          callback();
        })
      })
    });
  }
}
function initAdminButtons() {
  $(".special-links").remove();
   var div = $('<div />'),
      links = [
         ["Wypłaty", "#panel/admin/withdrawals"],
         ["Użytkownicy", "#panel/admin/users"],
      ];
   for(var i in links) {
     $('<li><a href="'+links[i][1]+'" title="'+links[i][0]+'">'+links[i][0]+'</a></li>').appendTo(div);
   }
   div.addClass('special-links').appendTo('#content .nav');
}

function timeConverterFromDate(date, type) {
  var date = new Date(date);
  console.info(date);
  var time = (date.getTime()) /1000;
  return timeConverter(time, type);
}

function timeConverter(UNIX_timestamp, type){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paź','Lis','Gru'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  if(min < 10) min = '0' + min;
  if(month < 10) month = '0' + month;
  if(date < 10) date = '0' + date;
  if(sec < 10) sec = '0' + sec;
  if(hour < 10) hour = '0' + hour;
  
  switch(type)
  {
    default:
      return date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
      break;
    case 'date':
      return date + ' ' + month + ' ' + year;
      break;
    case 'time':
      return hour + ':' + min + ':' + sec ;
      break;
  }
  
}

function setActive(name) {
  $(".active").removeClass("active");
  $('a[href$="#panel/'+name+'"]').parent().addClass("active");
}

$(".nav a").on("click", function() {
  $(".active").removeClass("active");
  $(this).parent().addClass("active");
})