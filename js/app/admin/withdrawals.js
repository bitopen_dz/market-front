if (getCookie("Authorization").length == 0) {
    window.location.hash = "";
}
setActive("dashboard");
$(function () {
    masterInit(init);
    //init();
});
var withdrawals = {};
function init() {
    var status = "PENDING",
        currency = $("#currency").val();
    if (currency == "PLN") {
        $("#send-withdrawals").show();
    }
    else {
        $("#send-withdrawals").hide();
    }
    rest.get("/admin/withdrawals/get/" + currency + "/" + status, function (data) {
        withdrawals = {};
        if (data.result.length == 0) {
            $("#withdrawals-table tbody").html('<tr><td colspan="4">Obecnie brak przelewów ' + currency + ' do potwierdzenia</td></tr>')
        }
        $("#withdrawals-table tbody").html("")
        for (var i in data.result) {
            var item = data.result[i];
            withdrawals[item.withdrawalId] = item;
            $('<tr />')
                .append('<td>' + timeConverterFromDate(item.created) + '</td>')
                .append('<td>' + item.amount + '</td>')
                .append('<td>' + item.accountNumber + '</td>')
                .append('<td><button class="btn btn-success btn-xs make-action" data-id="' + item.withdrawalId + '">Wykonaj</button> ' +
                    '<button class="btn btn-danger btn-xs cancel-action" data-id="' + item.withdrawalId + '">Anuluj</button></td>')
                .appendTo("#withdrawals-table tbody");
        }
        $(".make-action").on("click", function () {
            var id = $(this).data("id"),
                item = withdrawals[id];
            if (Currencies[item.currencyId].crypto) {
                processCryptoAction(item);
            } else {
                processFiatAction(item);
            }
        })
        $(".cancel-action").on("click", function () {
            var id = $(this).data("id");
            bootbox.confirm("Czy na pewno chcesz oznaczyć wypłatę jako niewykonaną?", function (result) {
                if (result) {
                    setItemStatus(id, "CANCELLED");
                }
            })

        })
    })
}

$("#currency").on("change", init);

function setItemStatus(id, status) {
    rest.post("/admin/withdrawals/status/" + id, function (data) {
        init();
    }, {status: status});
}

function processCryptoAction(item) {
    bootbox.confirm("<h4>Wypłata " + item.amount + " " + item.currencyId + " na adres " + item.accountNumber
        + '</h4><img src="https://chart.googleapis.com/chart?chs=202x202&cht=qr&chld=L|1&choe=UTF-8&chl=bitcoin:' + item.accountNumber + '?amount=' + item.amount + '" />', function (result) {
        if (result) {
            setItemStatus(item.withdrawalId, "SUCCESS");
        }
    });
}
function processFiatAction(item) {
    //window.open()
    bootbox.confirm("Czy przelew został wykonany?", function (result) {
        if (result) {
            setItemStatus(item.withdrawalId, "SUCCESS");
        }
    });
}

$("#send-withdrawals").on("click", function() {
   var ids = [];
   $(".make-action").each(function() {
       ids.push($(this).data("id"));
   })
    window.open("http://bitopen.pl:9999/admin/withdrawals/file/"+getCookie("Authorization")+"/"+ids.join(","));
});