if (getCookie("Authorization").length == 0) {
    window.location.hash = "";
}
setActive("dashboard");
$(function () {
    masterInit(init);
    //init();
});
var currentId = 0;
function init() {
    var filter = $("#users-filter").val();
    rest.get("/admin/users/find?email=" + filter, function (data) {
        $("#users-table tbody").html("");
        for (var i in data.result) {
            var user = data.result[i];
            $('<tr />')
                .append('<td>' + user.email + '</td>')
                .append('<td>' + user.status + '</td>')
                .append('<td>' + timeConverterFromDate(user.created) + '</td>')
                .append('<td><button class="btn btn-default btn-xs edit-user" data-id="' + user.userId + '">Edytuj</button></td>')
                .appendTo("#users-table")
        }

        $(".edit-user").on("click", function (e) {
            e.preventDefault();
            currentId = $(this).data("id");
            rest.get("/admin/users/" + currentId, function (result) {

                bootbox.confirm("<h3>Edycja użytkownika</h3>" + getUserDataForm(result.result), function (result) {
                    if (result) {
                        saveUser();
                    }
                });
                setTimeout(function() {
                    $('[name="status"]').val(result.result.user.status);
                }, 50);
            })
        });
    })
}

$("#users-filter").on("change", init);

function getBaseDataForm(user) {
    return ' Adres e-mail: <input type="text" class="form-control" name="email" value="'+user.email+'"/> ' +
        'Status: <select name="status" class="form-control"><option>created</option><option>active</option><option>locked</option><option>removed</option></select>';
}

function getDetailsForm(details) {
    details = details || {};
    return "<hr />" +
        "<strong>Imię i nazwisko:</strong> "+(details.firstname || "Brak") + " " + (details.lastname || "") + "<br />" +
        "<strong>Adres zamieszkania:</strong>  " + (details.street1 || "Brak") + " "+(details.street2 || "") + ", " + (details.postcode || "") + " "+(details.city || "") + ", "+(details.country || "")+"<br />" +
        "<strong>Numer dokumentu:</strong> "+ (details.idcardNumber || "");
}

function getUserDataForm(result) {
    return getBaseDataForm(result.user) + getDetailsForm(result.details);
}

function saveUser() {
    var input = collectData();
    rest.isJson = true;
    rest.post("/admin/users/" + currentId, function(data) {
        success("Dane zostały zapisane");
        init();
    }, input );
}

function collectData() {
    var result = {};
    $("input, select", ".bootbox-body").each(function() {
        result[ $(this).attr("name") ] = $(this).val();
    });
    return result;
}