
setActive("settings");
var fields = ["firstname", "lastname", "street1", "street2", "city", "country", "postcode", "idcardNumber"];
$(function() {
  masterInit(init);
})

function init() {
  load2FAStatus();
  loadUserDetails();
}

function load2FAStatus() {
  rest.get("/users/twoFactor", function(data) {
    if(data.result) {
      $("#two-step-verification-status").html('<button class="btn btn-danger" id="disable-2fa">Wyłącz</button>');
    }
    else {
      $("#two-step-verification-status").html('<button class="btn btn-success" id="enable-2fa">Włącz</button>');
    }
  });
}

function loadUserDetails() {
  rest.get("/users/details", function(response){
    if(response.result != null) {
      for(var i in fields) {
        $("#user-details-form").find('[name="'+fields[i]+'"]').val(response.result[fields[i]]);
      }
    }
  });
}

$(document).off("click", "#disable-2fa").on("click", "#disable-2fa", function(e) {
  e.preventDefault();
  $(this).addClass("disabled").attr("disabled", true);
  rest.post("/users/twoFactor/disable", function() {
    load2FAStatus();
  });
})
$(document).off("click", "#enable-2fa").on("click", "#enable-2fa", function(e) {
  e.preventDefault();
  $(this).addClass("disabled").attr("disabled", true);
  rest.post("/users/twoFactor", function(data) {
    load2FAStatus();
    bootbox.alert("<div style='text-align: center;'>Zeskanuj poniższy kod QR w aplikacji Google Authenticator. "+
                  "Dzięki niemu aplikacja będzie generować czasowy token, o który będziemy Cię prosić przy logowaniu na Twoje, aktualizacji danych oraz zlecaniu wypłat. "+
                  "W przypadku podania nieprawidłowego tokenu, wykonanie konkretnej akcji będzie niemożliwe."+
                  "<img src='https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=otpauth://totp/BitOpen.pl:"
                  +$("#login-info").text()+"?secret="+data.result+"&issuer=BitOpen' /><br />Możesz również przepisać klucz ręcznie: "
                  +data.result+"</div>");
  });
});


$("#user-details-form").on("submit", function(e) {
  e.preventDefault();
  rest.isJson = true;
  var data = {};
  
  for (var i in fields) {
    data[fields[i]] = $(this).find('[name="'+fields[i]+'"]').val();
  }
  rest.post("/users/details", function(response) {
    if(response.status == 1){
      success("Dane zostały zapisane");
    }
  }, data);
});
$("#change-password-form").on("submit", function(e) {
  e.preventDefault();
  var password = $("#new-password").val(),
      passwordRepeat = $("#new-password-repeat").val();
  rest.post("/users/changePassword", function(response) {
    if(response.status == 1){
      success("Hasło zostało poprawnie zmienione");
    }
  }, {password: password, passwordRepeat: passwordRepeat});
});