if(getCookie("Authorization").length == 0){
        window.location.hash = "";
}
setActive("history");
$(function() {
        masterInit(init);
        //init();
});

var types = {
        "deposit": {
                comment: "Przelew",
                ico: "arrow-circle-o-down"
        },
        "currency_tranasction": {
                comment: "Transakcja",
                ico: "refresh"
        }
}
var currentHistoryPage = 1;
function init(){
        loadHistory();
        rest.get("/wallets/historyCount", function(data) {
                var count = Math.ceil(data.result);
                for(var i = 1; i <= count; i++) {
                        $("#pagination").append('<button class="change-page btn '+(currentHistoryPage == i ? 'btn-primary' : 'btn-default')+'" data-page="'+i+'">'+i+'</button>')
                }
                $(".change-page").on("click", function(e) {
                        e.preventDefault();
                        currentHistoryPage = $(this).data("page");
                        $("#history-table").css("opacity", 0.5);
                        $(this).parent().find(".btn-primary").removeClass("btn-primary").addClass("btn-default");
                        $(this).addClass("btn-primary").removeClass("btn-default");
                        loadHistory();
                });
        });
}
function loadHistory() {
        
        rest.get("/wallets/history/"+currentHistoryPage, function(data){
                $("#history-table").css("opacity", 1);
                $("#history-table tbody").html("")
                for(var i in data.result){
                        var item = data.result[i];
                        $('<tr />')
                                .append('<td><i class="fa fa-'+types[item.comment].ico+'"></i></td>')
                                .append('<td>'+types[item.comment].comment+'</td>')
                                .append('<td><strong class="'+((item.amount > 0) ? 'positive' : 'negative')+'">'+display(item.amount, item.wallet.currencyId)+'</strong></td>')
                                .append('<td>'+display(item.balanceAfter, item.wallet.currencyId)+'</td>')
                                .append('<td>'+timeConverter(item.created/1000)+'</td>')
                                .appendTo("#history-table tbody");
                }
        });
}


