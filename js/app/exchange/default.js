var priceCurrency = "PLN",
    cryptoCurrency = "BTC",
    socket = null;
if(getCookie("Authorization").length == 0){
  window.location.hash = "";
}
setActive("exchange");
$(function() {
  masterInit(init);
  //init();
});

var insertAnimation = false;
function init(){
  rest.get("/orders/orderbook/BTCPLN", function(data) {
    $("#bid-table tbody").html("");
    $("#ask-table tbody").html("");
    result = data.result;
    for(var i in result.bid){
      insertOffer("bid", result.bid[i]);
      if($("#ask-rate").val().length == 0) {
        $("#ask-rate").val(parseFloat(result.bid[i].rate).toFixed(2));
      }
    }
    for(var i in result.ask){
      insertOffer("ask", result.ask[i]);
      if($("#bid-rate").val().length == 0) {
        $("#bid-rate").val(parseFloat(result.ask[i].rate).toFixed(2));
      }
    }
    insertAnimation = true;
  });
  rest.get("/orders/transactions/BTCPLN", function(data) {
    $("#transactions-table tbody").html("");
    result = data.result;
    for(var i in result){
      $("<tr />")
        .append("<td>"+timeConverter(result[i].created/1000)+"</td>")
        .append("<td>"+display(result[i].price / result[i].amount, priceCurrency)+"</td>")
        .append("<td>"+display(result[i].amount, cryptoCurrency)+"</td>")
        .append("<td>"+display(result[i].price, priceCurrency)+"</td>")
        .prependTo("#transactions-table tbody");
    }
  });
  refreshBalances();
  
  if(!socket){
    socket = new WebSocket("ws://bitopen.pl:9999/orders/orderbook");
    socket.onmessage = function(event){
      handleSocketMessage(JSON.parse(event.data));
    }
  }
}

function refreshBalances() {
  rest.get("/wallets", function(data){
    $("#wallets-list").html("");
    for(var i in data.result){
      var item = data.result[i];
      $('#'+item.currencyId+'-balance').text(display(item.balance - item.locked, item.currencyId));
    }
  });
}

var existsOffers = {};
function insertOffer(type, offer){
  existsOffers[offer.orderId] = offer;
  var existsTr = $("#"+type+"-table tbody tr[data-rate='"+offer.rate+"']");
  if(existsTr.length > 0){
    var amountTd = existsTr.find("td[data-role='amount']"),
        priceTd = existsTr.find("td[data-role='price']"),
        amountValue = parseFloat(amountTd.text().split(" ")[0]),
        priceValue = parseFloat(priceTd.text().split(" ")[0]);
        
    amountTd.text(display(amountValue + offer.amount, cryptoCurrency));
    priceTd.text(display(priceValue + offer.price, priceCurrency));
    
  }
  else
  {
    existsTr = $('<tr />')
      .attr("data-rate", offer.rate)
      .append('<td data-role="rate">'+display(offer.rate, priceCurrency)+'</td>')
      .append('<td data-role="amount">'+display(offer.amount, cryptoCurrency)+'</td>')
      .append('<td data-role="price">'+display(offer.price, priceCurrency)+'</td>')
      .appendTo("#"+type+"-table tbody");
  }
  if(insertAnimation){
    highlight(existsTr);
    if(offer.userId == User.userId){
      refreshBalances()
    }
  }
  if(offer.userId == User.userId) {
    applyToMyOffers(offer);
  }
}
var types = {'bid': 'Kupno', 'ask': 'Sprzedaż'};
function applyToMyOffers(offer){
  $('tr[data-id="'+offer.orderId+'"]').remove();
  if(offer.amount > 0)
  {
    $('<tr />')
      .attr("data-id", offer.orderId)
      .append('<td>'+types[offer.type]+'</td>')
      .append('<td>'+display(offer.rate, priceCurrency)+'</td>')
      .append('<td>'+display(offer.amount, cryptoCurrency)+'</td>')
      .append('<td>'+display(offer.rate * offer.amount, priceCurrency)+'</td>')
      .append('<td><button class="btn btn-danger btn-xs cancel-order" data-id="'+offer.orderId+'">Anuluj</button></td>')
      .appendTo('#offers-table');
  }
  if($("#offers-table tbody tr").length > 0)
  {
    $("#offers-div").show();
  }
  else
  {
    $("#offers-div").hide();
  }
}

function handleSocketMessage(data){
  switch(data.type){
    case "new_order":
      handleNewOrderMessage(data.content);
      break;
    case "offer-expired":
      handleOfferChanged(data.content);
      break;
    case "offer-changed":
      handleOfferChanged(data.content);
      break;
    case "new-transaction":
      handleNewTransaction(data.content);
      break;
    case "notification":
      warning(data.content);
      break;
  }
}
function handleNewTransaction(transaction) {
  var tr = $("<tr />")
    .append("<td>"+timeConverterFromDate(transaction.created)+"</td>")
    .append("<td>"+display(transaction.price / transaction.amount, priceCurrency)+"</td>")
    .append("<td>"+display(transaction.amount, cryptoCurrency)+"</td>")
    .append("<td>"+display(transaction.price, priceCurrency)+"</td>")
    .prependTo("#transactions-table tbody");
  highlight(tr);
  var myId = User.userId;
  if(transaction.buyerId == myId || transaction.sellerId == myId) {
    refreshBalances();
    if(transaction.buyerId == myId) {
      success("Kupiłeś "+transaction.amount+" "+cryptoCurrency+" za "+transaction.price+" "+priceCurrency+" ");
    }
    if(transaction.sellerId == myId) {
      success("Sprzedałeś "+transaction.amount+" "+cryptoCurrency+" za "+transaction.price+" "+priceCurrency+" ");
    }
  }
}
function handleOfferExpired(offer){
}

function handleOfferChanged(offer){
  var existsTr = $("#"+offer.type+"-table tbody tr[data-rate='"+offer.rate+"']");
  if(existsTr.length > 0){
    var changedAmount = existsOffers[offer.orderId].amount - offer.amount,
        changedPrice = existsOffers[offer.orderId].price - offer.price;
        
    existsOffers[offer.orderId].amount = offer.amount;
    existsOffers[offer.orderId].price = offer.price;
      
    var amountTd = existsTr.find("td[data-role='amount']"),
        priceTd = existsTr.find("td[data-role='price']"),
        amountValue = parseFloat(amountTd.text().split(" ")[0]),
        priceValue = parseFloat(priceTd.text().split(" ")[0]),
        newAmount = amountValue - changedAmount,
        newPrice = priceValue - changedPrice;
        
    if(newPrice == 0 || newAmount == 0){
      existsTr.remove();
      $('tr[data-id="'+offer.orderId+'"]').remove();
      if($("#offers-table tbody tr").length == 0) {
        $("#offers-div").hide();
      }
    }
    else{
      amountTd.text(display(newAmount, cryptoCurrency));
      priceTd.text(display(newPrice, priceCurrency));
      applyToMyOffers(offer);
    }
    highlight(existsTr);
  }
  
}

function highlight(item){
  item.addClass("highlight");
  setTimeout(function() {
    item.removeClass("highlight");
  }, 500);
}

function handleNewOrderMessage(offer){
  insertOffer(offer.type, offer);
}

$("#bid-action").on("submit", function(e) {
  e.preventDefault();
  var input = {
    "type": "bid",
    "rate": parseFloat($("#bid-rate").val()),
    "amount": parseFloat($("#bid-amount").val())
  };
  sendSocket(input);
});
$("#ask-action").on("submit", function(e) {
  e.preventDefault();
  var input = {
    "type": "ask",
    "rate": parseFloat($("#ask-rate").val()),
    "amount": parseFloat($("#ask-amount").val())
  };
  sendSocket(input);
});

$("#bid-amount").on("keyup", function() {
  var rate = parseFloat($("#bid-rate").val()),
      newVal = rate * $(this).val();
  if(!isNaN(rate) && rate > 0 && !isNaN(newVal)){
    $("#bid-price").val(newVal.toFixed(2))
  }
});

$("#bid-price").on("keyup", function() {
  var rate = parseFloat($("#bid-rate").val()),
      newVal = $(this).val() / rate;
  if(!isNaN(rate) && rate > 0 && !isNaN(newVal)){
    $("#bid-amount").val(newVal.toFixed(8))
  }
});

$("#ask-amount").on("keyup", function() {
  var rate = parseFloat($("#ask-rate").val()),
      newVal = rate * $(this).val();
  if(!isNaN(rate) && rate > 0 && !isNaN(newVal)){
    $("#ask-price").val(newVal.toFixed(2))
  }
});

$("#ask-price").on("keyup", function() {
  var rate = parseFloat($("#ask-rate").val()),
      newVal = $(this).val() / rate;
  if(!isNaN(rate) && rate > 0 && !isNaN(newVal)){
    $("#ask-amount").val(newVal.toFixed(8))
  }
});

$(".fill-ask-amount").on("click", function() {
  $("#ask-amount").val($(this).text().split(" ")[0]).trigger("keyup");
})
$(".fill-bid-price").on("click", function() {
  $("#bid-price").val($(this).text().split(" ")[0]).trigger("keyup");
})

function sendSocket(input){
  input.pairId = cryptoCurrency+priceCurrency;
  input.userToken = getCookie("Authorization");
  
  socket.send(JSON.stringify(input));
}

$(document).off("click", ".cancel-order").on("click", ".cancel-order", function(e) {
  e.preventDefault();
  var id = $(this).data("id");
  bootbox.confirm("Czy na pewno chcesz usunąć tę ofertę?", function(result) {
    if(result){
      socket.send('{"type": "cancel", "id": '+id+', "pairId": "'+cryptoCurrency+priceCurrency+'", "userToken": "'+getCookie("Authorization")+'"}');
      refreshBalances();
    }
  });
   
})

$(document).off("click", "#bid-table tr").on("click", "#bid-table tr", function(e) {
  e.preventDefault();
  $("#ask-rate").val($(this).find("td:nth-child(1)").text().split(" ")[0]);
  $("#ask-amount").val($(this).find("td:nth-child(2)").text().split(" ")[0]).trigger("keyup");
});
$(document).off("click", "#ask-table tr").on("click", "#ask-table tr", function(e) {
  e.preventDefault();
  $("#bid-rate").val($(this).find("td:nth-child(1)").text().split(" ")[0]);
  $("#bid-amount").val($(this).find("td:nth-child(2)").text().split(" ")[0]).trigger("keyup");
});