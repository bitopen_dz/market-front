setActive("wallet");

var parts = window.location.hash.split("/"),
        lastParts = parts[parts.length - 1].split("-"),
        WALLET_ID = 0,
        CURRENCY = "";
if(lastParts.length > 1){
        WALLET_ID = lastParts[1];
}

if(getCookie("Authorization").length == 0){
        window.location.hash = "";
}
$(function() {
        masterInit(init);
        //init();
});

var types = {
        "deposit": {
                comment: "Przelew",
                ico: "arrow-circle-o-down"
        }
}

function init(){
        rest.get("/wallets/id/"+WALLET_ID, function(data){
                $("#wallets-list").html("");
                var item = data.result;
                CURRENCY = item.currencyId;
                loadPendingList();
                $('<li data-currency="'+item.currencyId+'" data-id="'+item.walletId+'">'+
                        '<div class="accounts-list-item-basics col-md-7 col-sm-12">'+
                          '<strong>Konto '+item.currencyId+'</strong>'+
                          '<span class="hidden-xs">'+(item.address || "")+'</span>'+
                          '<strong class="visible-sm visible-xs pull-right">'+display(item.balance, item.currencyId)+'</strong>'+
                        '</div>'+
                        '<div class="accounts-list-item-balance col-md-5 hidden-sm hidden-xs">'+
                          '<span>Dostępne środki:</span>'+
                          '<strong>'+display(item.balance - item.locked, item.currencyId)+'</strong>'+
                          //'<em>'+item.currencyId+'</em>'+
                        '</div>'+
                '</li>')
                        .appendTo("#wallets-list");
                
                $("#available-balance").text( display(item.balance - item.locked, item.currencyId) );
                $("#wallet-balance").text( display(item.balance, item.currencyId) );
                $("#locked-balance").text( display(item.locked, item.currencyId) );
                $("#wallet-qr").attr("src", "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:"+item.address);
                $("#your-address").val(item.address);
                $("#wallet-details").show();
                loadWithdrawalsList();
                
        });
}

var lastPendingValue = -1,
        loadingTimeout = null;
function loadPendingList(){
        
        rest.get("/deposits/"+CURRENCY+"/PENDING", function(data){
                if($("#deposits-history tbody .el-success").length === 0){
                       $("#deposits-history tbody").html("");
                }
                $("#deposits-history tbody .el-pending").remove();
                for(var i in data.result){
                        var item = data.result[i];
                        console.info(item)
                        $('<tr />')
                                .addClass("el-pending")
                                .append('<td><i class="fa fa-spinner fa-pulse"></i></td>')
                                .append('<td><strong class="negative">Oczekuje ('+item.confirmations+'/1)</strong><br />'+timeConverter(item.created/1000)+'</td>')
                                .append('<td>'+display(item.amount, CURRENCY)+'</td>')
                                .append('<td><a href="http://tbtc.blockr.io/tx/info/'+item.txid+'" target="_blank">blockchain <i class="fa fa-external-link" /></a>')
                                .prependTo("#deposits-history tbody");
                }
                if(lastPendingValue != data.result.length)
                {
                        loadSuccessList();
                        if(lastPendingValue > 0){
                                init(); 
                        }
                        lastPendingValue = data.result.length;
                }
                if(loadingTimeout){
                        clearTimeout(loadingTimeout);
                }
                if(window.location.hash.search("wallet/crypto") > 0)
                {
                        loadingTimeout = setTimeout(loadPendingList, 1000);
                }
        }, null, false);
}


function loadSuccessList(){
        
        rest.get("/deposits/"+CURRENCY+"/SUCCESS", function(data){
                $("#deposits-history tbody .el-pending").remove();
                for(var i in data.result){
                        var item = data.result[i];
                        console.info(item)
                        $('<tr />')
                                .addClass("el-success")
                                .append('<td><i class="fa fa-arrow-circle-o-down"></i></td>')
                                .append('<td><strong class="positive">Wykonana</strong> <br />'+timeConverter(item.created/1000)+'</td>')
                                .append('<td>'+display(item.amount, CURRENCY)+'</td>')
                                .append('<td><a href="http://tbtc.blockr.io/tx/info/'+item.txid+'" target="_blank">blockchain <i class="fa fa-external-link" /></a>')
                                .appendTo("#deposits-history tbody");
                        if(i > 5){
                                break;
                        }
                }
        }, null, false);
}
function loadWithdrawalsList(){
        
        rest.get("/withdrawals/"+CURRENCY+"", function(data){
                $("#withdrawals-history tbody").html("");
                var resultReversed = data.result.reverse();
                for(var i in resultReversed){
                        var item = data.result[i];
                        var status = '<strong class="positive">Wykonana</strong>';
                        if(item.status == 'PENDING'){
                             status = '<strong class="negative">Oczekująca</strong>'   
                        }
                        $('<tr />')
                                .append('<td><i class="fa fa-arrow-circle-o-down"></i></td>')
                                .append('<td>'+status+'<br />'+timeConverter(item.created/1000)+'</td>')
                                .append('<td>'+item.accountNumber+'<br /> Kwota: '+display(item.amount, CURRENCY)+'</td>')
                                .appendTo("#withdrawals-history tbody");
                        if(i > 5){
                                break;
                        }
                }
        }, null, false);
}


$("#withdrawal-form").on("submit", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var amount = $(this).find("input[name='amount']").val(),
               accountNumber = $(this).find("input[name='accountNumber']").val(); 
        rest.post("/withdrawals", function(data) {
                loadWithdrawalsList();
        }, {"amount": amount, "currencyId": CURRENCY, accountNumber: accountNumber});
        
});