if(getCookie("Authorization").length == 0){
        window.location.hash = "";
}
setActive("wallet");
$(function() {
        masterInit(init);
        //init();
});

var types = {
        "deposit": {
                comment: "Wpłata",
                ico: "arrow-circle-o-down"
        }
}

function init(){
        rest.get("/wallets", function(data){
                $("#wallets-list").html("")
                for(var i in data.result){
                        var item = data.result[i];
                        $('<li data-currency="'+item.currencyId+'" data-id="'+item.walletId+'">'+
                                '<div class="accounts-list-item-basics col-md-7 col-sm-12">'+
                                  '<strong>Konto '+item.currencyId+'</strong>'+
                                  '<span class="hidden-xs">'+(item.address || "")+'</span>'+
                                  '<strong class="visible-sm visible-xs pull-right">'+display(item.balance, item.currencyId)+'</strong>'+
                                '</div>'+
                                '<div class="accounts-list-item-balance col-md-5 hidden-sm hidden-xs">'+
                                  '<span>Dostępne środki:</span>'+
                                  '<strong>'+display(item.balance - item.locked, item.currencyId)+'</strong>'+
                                  //'<em>'+item.currencyId+'</em>'+
                                '</div>'+
                        '</li>')
                                .appendTo("#wallets-list")
                                .on("click", function() {
                                        if(Currencies[$(this).data("currency")].crypto)
                                        {
                                                window.location.hash = "panel/wallet/crypto/id-"+$(this).data("id");
                                        }
                                        else
                                        {
                                                window.location.hash = "panel/wallet/fiat/id-"+$(this).data("id");
                                        }
                                });
                }
        })
}
