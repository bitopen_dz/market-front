setActive("wallet");
var parts = window.location.hash.split("/"),
        lastParts = parts[parts.length - 1].split("-"),
        WALLET_ID = 0,
        CURRENCY = "";
if(lastParts.length > 1){
        WALLET_ID = lastParts[1];
}

if(getCookie("Authorization").length == 0){
        window.location.hash = "";
}
$(function() {
        masterInit(init);
        //init();
});

var types = {
        "deposit": {
                comment: "Przelew",
                ico: "arrow-circle-o-down"
        }
}

function init(){
        rest.get("/wallets/id/"+WALLET_ID, function(data){
                $("#wallets-list").html("");
                var item = data.result;
                CURRENCY = item.currencyId;
                loadList();
                loadWithdrawalsList();
                $('<li data-currency="'+item.currencyId+'" data-id="'+item.walletId+'">'+
                        '<div class="accounts-list-item-basics col-md-7 col-sm-12">'+
                          '<strong>Konto '+item.currencyId+'</strong>'+
                          '<span class="hidden-xs">'+(item.address || "")+'</span>'+
                          '<strong class="visible-sm visible-xs pull-right">'+display(item.balance, item.currencyId)+'</strong>'+
                        '</div>'+
                        '<div class="accounts-list-item-balance col-md-5 hidden-sm hidden-xs">'+
                          '<span>Dostępne środki:</span>'+
                          '<strong>'+display(item.balance - item.locked, item.currencyId)+'</strong>'+
                          //'<em>'+item.currencyId+'</em>'+
                        '</div>'+
                '</li>')
                        .appendTo("#wallets-list");
                
                $("#available-balance").text( display(item.balance - item.locked, item.currencyId) );
                $("#wallet-balance").text( display(item.balance, item.currencyId) );
                $("#locked-balance").text( display(item.locked, item.currencyId) );
                //$("#wallet-qr").attr("src", "http://blockr.io/api/v1/address/Qr/"+item.address);
                //$("#your-address").val(item.address);
                $("#wallet-details").show();
                
        })
}


var lastPendingValue = -1,
        loadingTimeout = null;
function loadList(){
        
        rest.get("/deposits/"+CURRENCY+"", function(data){
                $("#deposits-history tbody").html("");
                
                var resultReversed = data.result.reverse();
                for(var i in resultReversed){
                        var item = data.result[i];
                        var status = '<strong class="negative">Oczekuje</strong>';
                        if(item.status == 'SUCCESS') {
                                status = '<strong class="positive">Wykonana</strong>'
                        }
                        $('<tr />')
                                .append('<td>'+status+' <br/>'+timeConverter(item.created/1000)+'</td>')
                                .append('<td>'+display(item.amount, CURRENCY)+'</td>')
                                .prependTo("#deposits-history tbody");
                }
        }, null, false);
}

$("#deposit-form").on("submit", function(e) {
        e.preventDefault();
        var amount = $(this).find("input[type='number']").val();
        rest.post("/deposits/dotpay/start", function(data){ $("body").append(data.result); }, {amount: amount});
})


function loadWithdrawalsList(){
        
        rest.get("/withdrawals/"+CURRENCY+"", function(data){
                $("#withdrawals-history tbody").html("");
                var resultReversed = data.result.reverse();
                for(var i in resultReversed){
                        var item = data.result[i];
                        var status = '<strong class="positive">Wykonana</strong>';
                        if(item.status == 'PENDING'){
                             status = '<strong class="negative">Oczekująca</strong>'   
                        }
                        $('<tr />')
                                .append('<td><i class="fa fa-arrow-circle-o-down"></i></td>')
                                .append('<td>'+status+'<br/>'+timeConverter(item.created/1000)+'</td>')
                                .append('<td>'+item.accountNumber+'<br />Kwota: '+display(item.amount, CURRENCY)+'</td>')
                                .appendTo("#withdrawals-history tbody");
                        if(i > 5){
                                break;
                        }
                }
        }, null, false);
}

$("#withdrawal-form").on("submit", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var amount = $(this).find("input[name='amount']").val(),
               accountNumber = $(this).find("input[name='accountNumber']").val(); 
        rest.post("/withdrawals", function(data) {
                loadWithdrawalsList();
        }, {"amount": amount, "currencyId": CURRENCY, accountNumber: accountNumber});
        
});