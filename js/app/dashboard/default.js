if(getCookie("Authorization").length == 0){
        window.location.hash = "";
}
setActive("dashboard");
$(function() {
        masterInit(init);
        //init();
});

var types = {
        "deposit": {
                comment: "Przelew",
                ico: "arrow-circle-o-down"
        },
        "currency_tranasction": {
                comment: "Transakcja",
                ico: "refresh"
        }
}

function init(){
        rest.get("/wallets", function(data){
                if(data.result.length == 0) {
                        success("Trwa generowanie portfeli...");
                        rest.post("/wallets/PLN", function(){
                                rest.post("/wallets/BTC", function() {
                                        init();
                                })
                        })
                        return;
                }
                $("#wallets-list").html("")
                for(var i in data.result){
                        var item = data.result[i];
                        $('<li data-currency="'+item.currencyId+'" data-id="'+item.walletId+'">'+
                                '<div class="accounts-list-item-basics col-md-7 col-sm-12">'+
                                  '<strong>Konto '+item.currencyId+'</strong>'+
                                  '<span class="hidden-xs">'+(item.address || "")+'</span>'+
                                  '<strong class="visible-sm visible-xs pull-right">'+display(item.balance, item.currencyId)+'</strong>'+
                                '</div>'+
                                '<div class="accounts-list-item-balance col-md-5 hidden-sm hidden-xs">'+
                                  '<span>Dostępne środki:</span>'+
                                  '<strong>'+display(item.balance - item.locked, item.currencyId)+'</strong>'+
                                  //'<em>'+item.currencyId+'</em>'+
                                '</div>'+
                        '</li>')
                                .appendTo("#wallets-list")
                                .on("click", function() {
                                        if(Currencies[$(this).data("currency")].crypto)
                                        {
                                                window.location.hash = "panel/wallet/crypto/id-"+$(this).data("id");
                                        }
                                        else
                                        {
                                                window.location.hash = "panel/wallet/fiat/id-"+$(this).data("id");
                                        }
                                });
                }
                rest.get("/wallets/history/1", function(data){
                        $("#history-table tbody").html("")
                        for(var i in data.result){
                                var item = data.result[i];
                                console.info(item)
                                $('<tr />')
                                        .append('<td><i class="fa fa-'+types[item.comment].ico+'"></i></td>')
                                        .append('<td>'+types[item.comment].comment+'</td>')
                                        .append('<td><strong class="'+((item.amount > 0) ? 'positive' : 'negative')+'">'+display(item.amount, item.wallet.currencyId)+'</strong></td>')
                                        .append('<td>'+timeConverter(item.created/1000)+'</td>')
                                        .appendTo("#history-table tbody");
                                //console.warn($("#history-table tbody").html());
                                if(i > 5) {
                                        break;
                                }
                        }
                })
        })
}
