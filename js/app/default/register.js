if(getCookie("Authorization").length > 0){
        window.location.hash = "panel/dashboard";
}
$(function() {
        $("#register-form").on("submit", function(e) {
                e.preventDefault();
                e.stopPropagation();
                NProgress.start();
                var email = $("#register-login").val(),
                        password = $("#register-password").val(),
                        passwordRepeat = $("#register-repeat-password").val();
                var input = {
                        email: email,
                        password: password,
                        passwordRepeat: passwordRepeat
                }
                rest.post("/auth/register", function(data) {
                        if(data.status){
                                window.location.hash = "#index/default";
                                success("Na adres e-mail otrzymasz link aktywujący. Kliknij w niego, by potwierdzić rejestrację konta.");
                        }
                }, input)
        });
});