if(getCookie("Authorization").length > 0){
        window.location.hash = "panel/dashboard";
}
$(function() {
        $("#login-form").on("submit", function(e) {
                e.preventDefault();
                e.stopPropagation();
                NProgress.start();
                var email = $("#login-login").val(),
                        password = $("#login-password").val();
                rest.authorize(email, password, function() {
                        window.location.hash = "panel/dashboard";
                })
        });
});